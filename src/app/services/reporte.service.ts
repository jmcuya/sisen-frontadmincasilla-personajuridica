import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { convertObjectToGetParams } from '../utils/http-utils';

const API_URL = environment.URL_SERVICES;
const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
    }),
};
@Injectable({
    providedIn: 'root'
})
export class ReporteService {
    constructor(private http: HttpClient) { }

    reporteCasillas(request: { fechaInicio: any, fechaFin: any }): Observable<any> {
        return this.http
            .get<any>(API_URL + '/reporte/casillas', {
                params: convertObjectToGetParams(request),
                responseType: 'blob' as 'json'
            }).pipe(map((res) => res));
    }
    
    reporteNotificaciones(request: { fechaInicio: any, fechaFin: any }): Observable<any> {
        return this.http
            .get<any>(API_URL + '/reporte/notificaciones', {
                params: convertObjectToGetParams(request),
                responseType: 'blob' as 'json'
            }).pipe(map((res) => res));
    }
}


