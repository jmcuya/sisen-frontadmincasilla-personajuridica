import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ReporteService } from 'src/app/services/reporte.service';
import { FuncionesService } from 'src/app/utils/funciones.service';
import * as moment from 'moment';
import { DateAdapter, MAT_DATE_LOCALE } from '@angular/material/core';
export enum TipoReporteEnum {
  REPORTE_CASILLAS = 1,
  REPORTE_NOTIFICACIONES = 2,
}
@Component({
  selector: 'app-pop-op-reportes',
  templateUrl: './pop-op-reportes.component.html',
  styleUrls: ['./pop-op-reportes.component.scss']
})
export class PopOpReportesComponent implements OnInit {
  tipoReporte: TipoReporteEnum;
  isLoading: boolean = false;
  fechaInicio: Date = null;
  fechaFin: Date = null;
  minDatefi = new Date();
  maxDatefi= new Date();
  minDateff = new Date();
  maxDateff= new Date();
  mensaje= null;
  constructor(private dialog: MatDialogRef<PopOpReportesComponent>,
    private reporteService: ReporteService,
    private _adapter: DateAdapter<any>,
    @Inject(MAT_DATE_LOCALE) private _locale: string,
    private funcionesService: FuncionesService) {
      this._locale = 'es';
      this._adapter.setLocale(this._locale);
     }

  ngOnInit(): void {
    this.minDatefi = new Date("2021-01-02");
    this.minDatefi.setDate(this.minDatefi.getDate()); // = new Date(currentYear,0,1); //.setFullYear(2000);
  }

  handleOk() {
    this.mensaje = null;

    if(!this.fechaInicio || !this.fechaFin){
      this.mensaje ='Debe seleccionar un rango de fechas para obtener el reporte';
      return false;
    }
    const promise = this.tipoReporte == TipoReporteEnum.REPORTE_CASILLAS ? this.reporteService.reporteCasillas({
      fechaInicio: this.fechaInicio ? moment(this.fechaInicio).locale("PE") : null,
      fechaFin: this.fechaFin ?
        moment(this.fechaFin).locale("PE") : this.fechaFin
    }) :
      this.reporteService.reporteNotificaciones({
        fechaInicio: this.fechaInicio ? moment(this.fechaInicio).locale("PE") : null,
        fechaFin: this.fechaFin ?
          moment(this.fechaFin).locale("PE") : this.fechaFin
      });
    var tmpTipoReporte = (this.tipoReporte==1)?'Casillas':'Notificaciones';
    this.isLoading = true;
    this.funcionesService.showloading('Generando Reporte de '+tmpTipoReporte,'Cargando...');
    promise.subscribe(resp => {
      this.isLoading = false;
      this.funcionesService.closeloading();
      this.funcionesService.downloadFile(resp, this.tipoReporte == TipoReporteEnum.REPORTE_CASILLAS ? 'ReporteCasillas.xlsx' : 'ReporteNotificaciones.xlsx');
      this.dialog.close(true);
    }, err => {
      this.isLoading = false;
      this.funcionesService.closeloading();
      this.funcionesService.mensajeError('Error en el servicio');
    });

  }
  handleCancelar() {
    this.dialog.close(false);
  }
  onValueChange(e){
    this.fechaFin = null;
    this.maxDateff = new Date(this.fechaInicio);
    this.maxDateff.setDate(this.maxDateff.getDate()+31);
  }
}
