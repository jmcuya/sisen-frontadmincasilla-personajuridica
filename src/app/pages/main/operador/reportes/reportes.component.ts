import { Component, OnInit } from '@angular/core';
import { MatDialog } from "@angular/material/dialog";
import { PopOpReportesComponent, TipoReporteEnum } from "./pop-op-reportes/pop-op-reportes.component";

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.scss']
})
export class ReportesComponent implements OnInit {
  tipoReporte= TipoReporteEnum;

  constructor(
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }
  //
  opFiltroReportes(tipoReporte:TipoReporteEnum) {
    const popDetalle = this.dialog.open(PopOpReportesComponent, {
      width: '80%',
      maxWidth: '600px',
      disableClose: false
    });
    popDetalle.componentInstance.tipoReporte =  tipoReporte;
  }
  //
}
