import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { FuncionesService } from 'src/app/utils/funciones.service';
import { Location } from '@angular/common';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-solicitud-detail-pj',
  templateUrl: './solicitud-detail-pj.component.html',
  styleUrls: ['./solicitud-detail-pj.component.scss']
})
export class SolicitudDetailPjComponent implements OnInit {
  load = false;
  sinDatos = true;
  mostrarImagen = true;

  displayedColumns: string[] = ['dni', 'nombre', 'fec_reg', 'fec_eval', 'evaluador', 'fec_ini', 'fec_fin', 'sustento', 'accion'];

  id;
  motivo1_detalle = "DNI no corresponde al usuario";
  motivo2_detalle = "Documento de representante no corresponde al representante legal";
  motivo3_detalle = "Documento de representante no corresponde al personero legal (titular)";
  motivo4_detalle = "Documento adjunto ilegible o en blanco";
  motivo5_detalle = "Documento de representación mayor a 90 días calendarios";
  motivo6_detalle = "Foto adjunta no cumple con las especificaciones";
  motivo7_detalle = "Foto adjunta no corresponde a los datos registrados";
  motivo8_detalle = "Información registrada errada (ejm. colocan lisuras en el campo de la dirección)";
  motivo9_detalle = "otros";
  data: any = {};
  representante: any = {};

  imageURlss: any = '';

  constructor(
    private usuarioService: UserService,
    private router: ActivatedRoute,
    private route :Router,
    private funcionesService: FuncionesService,
    public domSanitizer: DomSanitizer,
    private readonly location: Location
  ) {
    this.id = this.router.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.getDataUser();
  }

  async getDataUser() {
    this.load = true;
    const info = await this.usuarioService.getUserDetailPJ(this.id, true).toPromise();
this.load = false;
    if (!info) return;
    this.data = info.user;
    this.sinDatos = false;
    if(this.data.enAtencion == true || this.data.enAtencion == undefined){

      this.funcionesService.mensajeInfo("El registro ya está siendo atendido por "+ this.data.enAtencionPor) .then((resp) => {

        this.usuarioService.searchListuser({search:"",filter : "",page:1,count:5,estado:"",fechaInicio:"",fechaFin:"",ordenFec:"desc"});
        this.linkRedirect('list-boxes')

    })
    .catch((err) => {});


    }

    console.log('informacion', info);

    this.representante = this.data.representante;
    console.log('DATA-INFO', this.data);
    console.log('cantidad de representantessssss',this.data.representantes.length );

    if(this.data.representantes.length > 1){
      this.mostrarImagen = false;
    }

    if (this.data.imageDNI) {
      this.imageURlss = this._arrayBufferToBase64(this.data.imageDNI);
    }
  }

  linkRedirect(section: any) {
    this.route.routeReuseStrategy.shouldReuseRoute = () => false;
    this.route.navigate(['/main/' + section]);
  }

  download = async (path: string, name: string) => {
    try {
      this.load = true;
      const file = await this.usuarioService.download(path).toPromise();

      console.log('archivoo', file);
      this.funcionesService.downloadFile(file, name);
      this.load = false;
    } catch (error) {
      this.load = false;
    }
  };

  ViewImg(attachments): string {
    let typeArray = new Uint8Array(attachments.data);
    const STRING_CHAR = String.fromCharCode.apply(null, typeArray);
    let base64String = btoa(STRING_CHAR);

    let info = 'data:image/jpeg;base64,' + base64String;
    return info;
    //this.domSanitizer.bypassSecurityTrustUrl('data:image/jpg;base64,'+ base64String);
  }

  _arrayBufferToBase64(buffer) {
    var binary = '';
    var bytes = new Uint8Array(buffer.data);
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
    let info = 'data:image/jpeg;base64,' + window.btoa(binary);
    return info;
  }

  showloading(subtitle: string = null, title: string = null){    
    Swal.fire({
      title: title,
      html: subtitle,
      showConfirmButton: false,
      allowOutsideClick: false,
      timerProgressBar: true,
      onBeforeOpen: () => {
        Swal.showLoading();
      }
    })

  }
  
  closeloading(){
    Swal.close();
  }

  cancelar() {
    this.location.back();
  }
}
