import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import codePhone from 'src/app/constants/codePhone';
import { TypeDocument } from 'src/app/models/notifications/notification';
import { Departamento, Distrito, Provincia } from 'src/app/models/ubigeo';
import { UserDetail, UserDetailUpdate, UserDetailUpdatePn } from 'src/app/models/users/user';
import { UbigeoService } from 'src/app/services/ubigeo.service';
import { UserService } from 'src/app/services/user.service';
import { Profile } from 'src/app/transversal/enums/global.enum';
import { FuncionesService } from 'src/app/utils/funciones.service';
@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user1.component-v2.html',
  styleUrls: ['./edit-user1.component.scss']
})
export class EditUser1Component implements OnInit {
  departamentoList: Array<Departamento> = []
  provinciaList: Array<Provincia> = []
  distritoList: Array<Distrito> = []
  departamentoList_rep: Array<Departamento> = []
  provinciaList_rep: Array<Provincia> = []
  distritoList_rep: Array<Distrito> = []
  load = false;
  data2: any = {};
  displayedColumns: string[] = ['dni', 'nombre', 'fec_reg', 'fec_eval', 'evaluador', 'fec_ini', 'fec_fin', 'sustento', 'accion'];


  cargos = [
    {id: '1', value: 'Representante Legal'},
    {id: '2', value: 'Personero Legal Titular'},
  ]
  
  /*OLD cargos = [
    {id: '1', value: 'Tesorero'},
    {id: '2', value: 'Presidente'},
    {id: '3', value: 'Presidente de la OEC'},
    {id: '4', value: 'Fundador'},
    {id: '5', value: 'Representante Legal'},
    {id: '6', value: 'Secretario'},
    {id: '7', value: 'Presidente del Comité Electoral'},
    {id: '8', value: 'Otros'},
  ]*/

  tipoDocumentoAdjunto  = [
    {id: '1', value: 'Documento que acredita su representación en la Organización Política'},
    {id: '2', value: 'Documento que acredita su representación en la Institución Pública'},
    {id: '3', value: 'Certificado de vigencia de poder en la Empresa privada'},
    {id: '4', value : 'Otros'}
  ]
  /*OLD tipoDocumentoAdjunto  = [
    {id: 'r', value: 'Resolución de designación'},
    {id: 'h', value: 'Historial de afiliación - ROP'},
    {id: 'o' , value : 'Otros'}
  ]*/

  isRepresentative:boolean = false;
  inputDisabled = false;
  documentTypeSelected: string = '';
  maxlengthNumDoc: number;
  placeHolder = 'Ingrese número de documento';
  user : any ;
  esInterno: boolean = false;
  id;
  public data: any
  codePhone = codePhone;
  codePhoneSelected: string;
  Formulario: FormGroup = this.fb.group({
    fm_optiontipo: this.fb.control({
      value: '',// this.data ? this.data.doc_type : '',
      disabled: true,
    }),
    fm_numerodoc: this.fb.control(
      {
        value: '',//this.data ? this.data.doc : '',
        disabled: true,
      },
      [Validators.pattern('^[0-9]+$')]
    ),
    //fm_phone_code: this.fb.control({ value:'' }, [Validators.pattern("^[0-9]*$")]),
    fm_phone: this.fb.control({ value:'' }, [Validators.pattern("^[0-9]*$")]),
    fm_paginaweb: this.fb.control({ value:'' }, [Validators.pattern("(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?")]),
    fm_nombres: this.fb.control({
      value:'',// this.data ? this.data.name : '',
      disabled: this.inputDisabled 
    }),
    fm_organization_name: this.fb.control({
      value:'',
      disabled: true
    }),
    fm_apellidoPaterno: this.fb.control({
      value:'',// this.data ? this.data.lastname : '',
      disabled: this.inputDisabled 
    }),
    fm_apellidoMaterno: this.fb.control({
      value:'',// this.data ? this.data.second_lastname : '',
      disabled: this.inputDisabled 
    }),
    fm_correo: this.fb.control(
      {
        value:'',// this.data ? this.data.email : '',
        disabled: this.inputDisabled 
      },
      [
        Validators.required,
        Validators.pattern(
          '[a-zA-Z0-9.+-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}'
        ),
      ]
    ),
    fm_correo_rep:this.fb.control(
      {
        value:'',// this.data ? this.data.email : '',
        disabled: this.inputDisabled 
      },
      [
        Validators.required,
        Validators.pattern(
          '[a-zA-Z0-9.+-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}'
        ),
      ]
    ),
    fm_celular_rep:this.fb.control(
      {
        value: '',//this.data ? this.data.profile : '',
        disabled: this.inputDisabled 
      },
      [Validators.required]
    ),
    fm_telefono: this.fb.control(
      {
        value: '',//this.data ? this.data.profile : '',
        disabled: this.inputDisabled 
      },
      [Validators.required]
    ),
    fm_direccion: this.fb.control(
      {
        value: '',//this.data ? this.data.profile : '',
        disabled: this.inputDisabled 
      },
      [Validators.required]
    ),
    fm_direccion_rep: this.fb.control(
      {
        value: '',//this.data ? this.data.profile : '',
        disabled: this.inputDisabled 
      },
      [Validators.required]
    ),
    fm_departamento: this.fb.control(
      {
        value: '',//this.data ? this.data.profile : '',
     
      },
      [Validators.required]
    ),
    fm_departamento_rep: this.fb.control(
      {
        value: '',//this.data ? this.data.profile : '',
        
      },
      [Validators.required]
    ),
    fm_provincia: this.fb.control(
      {
        value: '',//this.data ? this.data.profile : '',
        
      },
      [Validators.required]
    ),
    fm_provincia_rep: this.fb.control(
      {
        value: '',//this.data ? this.data.profile : '',
        
      },
      [Validators.required]
    ),
    fm_distrito: this.fb.control(
      {
        value: '',//this.data ? this.data.profile : '',
       
      },
      [Validators.required]
    ),
    fm_distrito_rep: this.fb.control(
      {
        value: '',//this.data ? this.data.profile : '',
      
      },
      [Validators.required]
    ),
    fm_cargo: this.fb.control(
      {
        value: '',
        disabled: this.inputDisabled 
      },
      [Validators.required]
    ),
    fm_tipoDocumentoAdjunto: this.fb.control(
      {
        value: '',
        disabled: this.inputDisabled 
      },
      [Validators.required]
    ),
    fm_rep_id: this.fb.control({value:'' }),
    fm_phone_rep: this.fb.control({value:'' }),
   // fm_phone_code_rep: this.fb.control({value:'' }, [Validators.pattern("^[0-9]*$")]),
  });
  type:string;
  action ?: boolean;
  constructor(
    //@Inject(MAT_DIALOG_DATA) public data: any,
    //private dialogRef: MatDialogRef<EditUser1Component>,
    private route: Router,
    private fb: FormBuilder,
    private userService: UserService,
    private funcionesService: FuncionesService,
    private ubigeoService: UbigeoService,
    private renderer: Renderer2,
    private router: ActivatedRoute,
    private usuarioService: UserService,
    @Inject(DOCUMENT) document : any
  ) {
    this.id = this.router.snapshot.paramMap.get('id');
    this.type =  this.router.snapshot.paramMap.get('type');
 
    var action = this.route.url.split("/")[3];
    if(action === 'edit'){
      this.action = false;
    }else{
      this.action = true;
    }

    console.log("action", this.action)
  }

  ngOnInit(): void {
   // this.getInfo();
    this.initForm();
    if(this.type === 'pj'){
      this.getInfoPj();
    }else{
      this.getInfoPn();
    }
    // if(this.data.estate_inbox == 'Registro interno') this.esInterno = true;
  }

  typeDocument: TypeDocument[] = [
    { id: 'dni', value: 'DNI' },
    { id: 'ruc', value: 'RUC'},
    { id: 'ce', value: 'Carnet de Extranjería' },
  ];

  profiles: TypeDocument[] = [
    { id: Profile.RegistryOperator, value: 'Registrador' },
    { id: Profile.Notifier, value: 'Notificador' },
    { id: Profile.Administrador, value: 'Administrador' },
  ];

  formInvalid(control: string) {
    return (
      this.Formulario.get(control).invalid &&
      (this.Formulario.get(control).dirty ||
        this.Formulario.get(control).touched)
    );
  }



  async getInfoPj(){
    //const  id = this.data.inbox_id;
    //const  id2 = this.data.id;
    
    this.load = true;
    const info = await this.usuarioService.getUserDetailPJ(this.id, true).toPromise();
    this.load = false;
    if (!info) return;
    this.data2 = info.user;

    this.userService.getUserDetail(this.id, false).subscribe((resp)=>{

      if(!resp.success){
        return;
      }
      this.user = resp.user;
      this.isRepresentative = !(Object.keys(resp.user.representative).length === 0);
      console.log(this.isRepresentative)
      let tmpPhoneCode = "";
      let tmpPhone = "";
      if(this.user.phone!=undefined){
        const phoneUser = this.user.phone.split("-");
        tmpPhoneCode = phoneUser[0];
        tmpPhone = phoneUser[1];
      }
      this.Formulario.controls["fm_optiontipo"].setValue( this.user.doc_type);
      this.Formulario.controls["fm_numerodoc"].setValue( this.user.doc);
      this.Formulario.controls["fm_nombres"].setValue( this.user.name);
      this.Formulario.controls["fm_apellidoPaterno"].setValue( this.user.lastname);
      this.Formulario.controls["fm_apellidoMaterno"].setValue( this.user.second_lastname);
      this.Formulario.controls["fm_correo"].setValue( this.user.email);
      this.Formulario.controls["fm_telefono"].setValue( this.user.cellphone);
      this.Formulario.controls["fm_direccion"].setValue( this.user.address);
      this.Formulario.controls['fm_organization_name'].setValue( this.user.organization_name )
      //this.Formulario.controls['fm_phone_code'].setValue(tmpPhoneCode)
      //this.Formulario.controls['fm_phone'].setValue(tmpPhone)
      this.Formulario.controls['fm_phone'].setValue(this.user.phone)
      this.Formulario.controls['fm_paginaweb'].setValue(this.user.paginaweb)

      this.Formulario.controls['fm_tipoDocumentoAdjunto'].setValue(this.user.representative.document_type_attachment)
      let tmpPhoneCodeRep = "";
      let tmpPhoneRep = "";
      if(this.user.representative.phone!=undefined){
        const phoneRep = this.user.representative.phone.split("-");
        tmpPhoneCodeRep = phoneRep[0];
        tmpPhoneRep = phoneRep[1];
      }
      //this.Formulario.controls['fm_phone_code_rep'].setValue(tmpPhoneCodeRep)
      this.Formulario.controls['fm_phone_rep'].setValue(tmpPhoneRep)
      this.Formulario.controls['fm_correo_rep'].setValue(this.user.representative.email)
      this.Formulario.controls['fm_celular_rep'].setValue(this.user.representative.cellphone)
      this.Formulario.controls['fm_direccion_rep'].setValue(this.user.representative.address)
      this.Formulario.controls['fm_cargo'].setValue(this.user.representative.position)
      this.Formulario.controls['fm_rep_id'].setValue(this.user.representative._id)

      if(this.user.ubigeo != undefined){
        var cadenaUbigeo = this.user.ubigeo.split("/");
        var dep = cadenaUbigeo[0].trim();
        var prov = cadenaUbigeo[1].trim();
        var dis = cadenaUbigeo[2].trim();
        console.log("dep- prov- dis", dep + " - "+ prov + " - " + dis)  
        
        var foundProv = this.departamentoList.find( departamento => departamento.nodep == dep)
        this.Formulario.controls["fm_departamento"].setValue(foundProv);

        this.cambiarProvincia(prov,dis); 
      }
      if(this.isRepresentative && this.user.representative.ubigeo){
        var cadenaUbigeo = this.user.representative.ubigeo.split("/");
        var dep = cadenaUbigeo[0].trim();
        var prov = cadenaUbigeo[1].trim();
        var dis = cadenaUbigeo[2].trim();
        console.log("dep2- prov2- dis2", dep + " - "+ prov + " - " + dis)  

        var foundProv = this.departamentoList_rep.find( departamento => departamento.nodep == dep)
        this.Formulario.controls["fm_departamento_rep"].setValue(foundProv);
        this.cambiarProvinciaRep(prov,dis)
      }
 
     }, (error)=>{
       console.error(error)
     })   
  }
  getInfoPn(){
    //const  id = this.data.inbox_id;
    //const  id2 = this.data.id;

    this.userService.getUserDetailPn(this.id, false).subscribe((resp)=>{

      if(!resp.success){
        return;
      }
      this.user = resp.user;
      this.Formulario.controls["fm_optiontipo"].setValue( this.user.doc_type);
      this.Formulario.controls["fm_numerodoc"].setValue( this.user.doc);
      this.Formulario.controls["fm_nombres"].setValue(this.user.name);
      this.Formulario.controls['fm_nombres'].disable();
      this.Formulario.controls["fm_apellidoPaterno"].setValue( this.user.lastname);
      this.Formulario.controls['fm_apellidoPaterno'].disable();
      this.Formulario.controls["fm_apellidoMaterno"].setValue( this.user.second_lastname);
      this.Formulario.controls['fm_apellidoMaterno'].disable();
      this.Formulario.controls["fm_correo"].setValue( this.user.email);
      this.Formulario.controls["fm_telefono"].setValue( this.user.cellphone);
      
      let tmpPhoneCode = "";
      let tmpPhone = "";
      if(this.user.phone!=undefined){
        const phoneRep = this.user.phone.split("-");
        tmpPhoneCode = phoneRep[0];
        tmpPhone = phoneRep[1];
      }
      //this.Formulario.controls['fm_phone_code_rep'].setValue(tmpPhoneCodeRep)
      console.log('fonoooooo111 -->', this.user.phone)
      console.log('fonoooooo222 -->', tmpPhone)
      this.Formulario.controls['fm_phone'].setValue(this.user.phone)
      
      
      this.Formulario.controls["fm_direccion"].setValue( this.user.address);
      this.Formulario.controls['fm_organization_name'].setValue( this.user.organization_name )

      if(this.user.ubigeo != undefined){
        var cadenaUbigeo = this.user.ubigeo.split("/");
        var dep = cadenaUbigeo[0].trim();
        var prov = cadenaUbigeo[1].trim();
        var dis = cadenaUbigeo[2].trim();
        console.log("dep- prov- dis", dep + " - "+ prov + " - " + dis)  

      
        var foundProv = this.departamentoList.find( departamento => departamento.nodep == dep)
        this.Formulario.controls["fm_departamento"].setValue(foundProv);

        this.cambiarProvincia(prov,dis); 
      }else{
        console.log("Aquí entroooooooooo ASD");
        this.Formulario.controls['fm_departamento'].setValue(null)
        this.Formulario.controls['fm_provincia'].setValue(null)
        this.Formulario.controls['fm_distrito'].setValue(null)
      }
 
     }, (error)=>{
       console.error(error)
     })   
  }

  getDepartamento(){
    this.ubigeoService.getDepartamentoList().subscribe((resp)=>{
      this.departamentoList = resp;
      this.departamentoList_rep = resp
    })
  }

  newRepresentative(){
    this.route.navigateByUrl(`/main/user/edit/pj/${this.id}/representante`,{ state: this.user});
  }

  cambiarProvincia(prov = "" , dis = "" ){

    this.Formulario.get("fm_provincia")?.reset();
    this.Formulario.get("fm_distrito")?.reset();
    this.distritoList = [];

    var value  = this.Formulario.get('fm_departamento')?.value.ubdep;

    this.ubigeoService.getProvinciaList(value).subscribe((resp)=>{

      this.provinciaList = resp
      if(prov != "" && dis != ""){
      var foundProv = this.provinciaList.find( provincia => provincia.noprv == prov)
      this.Formulario.controls["fm_provincia"].setValue(foundProv);
        this.cambiarDistrito(dis);
      }
  
    })
  }

  cambiarProvinciaRep(prov = "" , dis = "" ){

    this.Formulario.get("fm_provincia_rep")?.reset();
    this.Formulario.get("fm_distrito_rep")?.reset();
    this.distritoList_rep = [];

    var value = this.Formulario.get('fm_departamento_rep')?.value.ubdep;

    this.ubigeoService.getProvinciaList(value).subscribe((resp)=>{

      this.provinciaList_rep = resp
      if(prov != "" && dis != ""){
        var foundProv = this.provinciaList_rep.find( provincia => provincia.noprv == prov)
        console.log("FOUNDDD:", foundProv)
        this.Formulario.controls["fm_provincia_rep"].setValue(foundProv);
        this.cambiarDistritoRep(dis);
      }
  
    })
  }
  

  cambiarDistrito(dis = ""){
    this.Formulario.get("fm_distrito")?.reset();
    var valueprovincia = this.Formulario.get('fm_provincia')?.value.ubprv;
    var valuedepar = this.Formulario.get('fm_departamento')?.value.ubdep;

    this.ubigeoService.getDistritoList(valuedepar, valueprovincia).subscribe((resp)=>{

      this.distritoList = resp
      if(dis != ""){
      var foundDist = this.distritoList.find( distrito => distrito.nodis == dis)
      this.Formulario.controls["fm_distrito"].setValue(foundDist);
      }
    })
  }
  cambiarDistritoRep(dis = ""){
    this.Formulario.get("fm_distrito_rep")?.reset();
    var valueprovincia = this.Formulario.get('fm_provincia_rep')?.value.ubprv;
    var valuedepar = this.Formulario.get('fm_departamento_rep')?.value.ubdep;

    this.ubigeoService.getDistritoList(valuedepar, valueprovincia).subscribe((resp)=>{
      this.distritoList_rep = resp
      if(dis != ""){
        var foundDist = this.distritoList_rep.find( distrito => distrito.nodis == dis)
        this.Formulario.controls["fm_distrito_rep"].setValue(foundDist);
      }
    })
  }


  saveEdit(){
    
    var _dept = this.Formulario.controls["fm_departamento"].value.nodep;
    var _prov = this.Formulario.controls["fm_provincia"].value.noprv;
    var _dist = this.Formulario.controls["fm_distrito"].value.nodis; 
    var _dept_rep = this.Formulario.controls["fm_departamento_rep"].value.nodep;
    var _prov_rep = this.Formulario.controls["fm_provincia_rep"].value.noprv;
    var _dist_rep = this.Formulario.controls["fm_distrito_rep"].value.nodis; 
    this.funcionesService.showloading('procesando...','Actualizando casilla electrónica');
    var userDet = new UserDetailUpdate ();
    userDet.userId = this.id; //this.data.inbox_id ;
    userDet.email = this.Formulario.controls["fm_correo"].value;
    userDet.cellphone = this.Formulario.controls["fm_telefono"].value;
    userDet.phone = this.Formulario.controls["fm_phone"].value//!=undefined?this.Formulario.controls["fm_phone_code"].value+"-"+ this.Formulario.controls["fm_phone"].value:"";
    userDet.address = this.Formulario.controls["fm_direccion"].value;
    userDet.ubigeo = _dept + " / "+ _prov + " / " + _dist; //this.user.ubigeo;//this.Formulario.controls[""].value;    
    userDet.webSite = this.Formulario.controls['fm_paginaweb'].value;
    userDet.rep = { id: undefined, email: undefined, cellphone:undefined, phone:undefined, ubigeo:undefined, address:undefined, position:undefined, positionName:undefined}
    userDet.personType = this.type

    userDet.rep.id = this.Formulario.controls['fm_rep_id'].value;
    userDet.rep.email = this.Formulario.controls['fm_correo_rep'].value;
    userDet.rep.cellphone = this.Formulario.controls['fm_celular_rep'].value;
    userDet.rep.phone = this.Formulario.controls["fm_phone_rep"].value//!=undefined?this.Formulario.controls["fm_phone_code_rep"].value+"-"+ this.Formulario.controls["fm_phone_rep"].value:"";
    userDet.rep.ubigeo = _dept_rep + " / "+ _prov_rep + " / " + _dist_rep;
    userDet.rep.address = this.Formulario.controls['fm_direccion_rep'].value;
    userDet.rep.position = this.Formulario.controls['fm_cargo'].value;
    userDet.rep.positionName = userDet.rep.position!=""? this.cargos[Number(this.Formulario.controls['fm_cargo'].value) - 1].value:"";
    //userDet!.user!.name = "owner"
    //userDet!.phone = this.codePhoneSelected + '-'+ this.Formulario.controls['fm_phone'].value;
    //userDet!.user!.lastname = "lastname owner"

    this.userService.editUserDetailUpdate(userDet).subscribe((resp)=>{
      if(!resp.success){        
        this.funcionesService.closeloading();
        this.funcionesService.mensajeError(
          resp.error
        );
      }else{
        this.funcionesService.closeloading();
        this.funcionesService.mensajeOk('Los datos de la casilla fueron actualizados con éxito');
        this.route.navigateByUrl("main/list-boxes")
      }
    })
  }



  saveEditPN(){
    var _dept = this.Formulario.controls["fm_departamento"].value.nodep;
    var _prov = this.Formulario.controls["fm_provincia"].value.noprv;
    var _dist = this.Formulario.controls["fm_distrito"].value.nodis;
    this.funcionesService.showloading('procesando...','Actualizando casilla electrónica');
    var userDet = new UserDetailUpdatePn();
    userDet.userId = this.id; //this.data.inbox_id ;
    userDet.email = this.Formulario.controls["fm_correo"].value;
    userDet.cellphone = this.Formulario.controls['fm_telefono'].value;
    userDet.address = this.Formulario.controls["fm_direccion"].value;
    userDet.ubigeo = _dept + " / "+ _prov + " / " + _dist; //this.user.ubigeo;//this.Formulario.controls[""].value;  
    userDet.personType = this.type
    //userDet!.user!.name = "owner"
    //userDet!.phone = this.codePhoneSelected + '-'+ this.Formulario.controls['fm_phone'].value;
    userDet!.phone = this.Formulario.controls['fm_phone'].value;
    //userDet!.user!.lastname = "lastname owner"

    this.userService.editUserDetailUpdatePn(userDet).subscribe((resp)=>{
      if(!resp.success){
        this.funcionesService.closeloading();
        this.funcionesService.mensajeError(
          resp.error
        );
      }else{
        this.funcionesService.closeloading();
        this.funcionesService.mensajeOk('Los datos de la casilla fueron actualizados con éxito');
        this.route.navigateByUrl("main/list-boxes");
      }
    })
  }

  eShowError = (input, error = null) => {
    if (error.required != undefined) {
      return 'Campo Requerido';
    } else if (error.pattern != undefined) {
      return 'Formato no válido';
    } else if (error.fileSize != undefined) {
      return 'Archivo(s) con peso excedido';
    } else if (error.minlength != undefined) {
      return 'Se requiere '+error.minlength.requiredLength+ ' caracteres como mínimo' ;
    } else {
      return 'Campo inválido';
    }
  };
  cambiarCargo(){
    console.log(this.Formulario)
  }

  initForm() {
    // this.Formulario = this.fb.group({
    //   fm_optiontipo: this.fb.control({
    //     value: '',// this.data ? this.data.doc_type : '',
    //     disabled: true,
    //   }),
    //   fm_numerodoc: this.fb.control(
    //     {
    //       value: '',//this.data ? this.data.doc : '',
    //       disabled: true,
    //     },
    //     [Validators.pattern('^[0-9]+$')]
    //   ),
    //   fm_nombres: this.fb.control({
    //     value:'',// this.data ? this.data.name : '',
    //     disabled: this.inputDisabled,
    //   }),
    //   fm_organization_name: this.fb.control({
    //     value:'',
    //     disabled: true
    //   }),
    //   // fm_apellidos: this.fb.control({
    //   //   value: this.data ? this.data.lastname : '',
    //   //   disabled: this.inputDisabled,
    //   // }),
    //   fm_apellidoPaterno: this.fb.control({
    //     value:'',// this.data ? this.data.lastname : '',
    //     disabled: this.inputDisabled,
    //   }),
    //   fm_apellidoMaterno: this.fb.control({
    //     value:'',// this.data ? this.data.second_lastname : '',
    //     disabled: this.inputDisabled,
    //   }),
    //   fm_correo: this.fb.control(
    //     {
    //       value:'',// this.data ? this.data.email : '',
    //       disabled: this.inputDisabled,
    //     },
    //     [
    //       Validators.required,
    //       Validators.pattern(
    //         '[a-zA-Z0-9.+-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}'
    //       ),
    //     ]
    //   ),
    //   fm_telefono: this.fb.control(
    //     {
    //       value: '',//this.data ? this.data.profile : '',
    //       disabled: this.inputDisabled,
    //     },
    //     [Validators.required]
    //   ),
    //   fm_direccion: this.fb.control(
    //     {
    //       value: '',//this.data ? this.data.profile : '',
    //       disabled: this.inputDisabled,
    //     },
    //     [Validators.required]
    //   ),
    //   fm_departamento: this.fb.control(
    //     {
    //       value: '',//this.data ? this.data.profile : '',
    //       disabled: false,
    //     },
    //     [Validators.required]
    //   ),
    //   fm_provincia: this.fb.control(
    //     {
    //       value: '',//this.data ? this.data.profile : '',
    //       disabled: false,
    //     },
    //     [Validators.required]
    //   ),
    //   fm_distrito: this.fb.control(
    //     {
    //       value: '',//this.data ? this.data.profile : '',
    //       disabled: false,
    //     },
    //     [Validators.required]
    //   ),
    // });
    // if(this.data.estate_inbox === 'Registro interno'){
    //   this.Formulario.get('fm_departamento').clearValidators();
    //   this.Formulario.get('fm_provincia').clearValidators();
    //   this.Formulario.get('fm_distrito').clearValidators();
    //   this.Formulario.get('fm_departamento').updateValueAndValidity();
    //   this.Formulario.get('fm_provincia').updateValueAndValidity();
    //   this.Formulario.get('fm_distrito').updateValueAndValidity();
    // }


   


    this.getDepartamento();

    if(this.action){
    this.Formulario.disable();
    console.log("disabled")
    }

    this.activateInactiveInputs();
  }


  activateInactiveInputs(){
 // var required = this.type =='pj' ? [Validators.required] : null;
  //pj-pn
  const a1 = this.Formulario.get('fm_phone');
  const a2 = this.Formulario.get('fm_paginaweb');
  const a3 = this.Formulario.get('fm_correo');
  const a4 = this.Formulario.get('fm_direccion');
  const a5 = this.Formulario.get('fm_departamento');
  const a6 = this.Formulario.get('fm_provincia');
  const a7 = this.Formulario.get('fm_distrito');
  const a8 = this.Formulario.get('fm_telefono');


  ///rep
  const a9 = this.Formulario.get('fm_celular_rep');
  const a10 = this.Formulario.get('fm_correo_rep'); 
  const a11 = this.Formulario.get('fm_direccion_rep');
  const a12 = this.Formulario.get('fm_departamento_rep');
  const a13 = this.Formulario.get('fm_provincia_rep');
  const a14 = this.Formulario.get('fm_distrito_rep');
  const a15 = this.Formulario.get('fm_tipoDocumentoAdjunto')
  const a16 = this.Formulario.get('fm_cargo');

  if(this.type === 'pn'){
    a1.setValidators(null);
    a2.setValidators(null);
    a9.setValidators(null);
    a10.setValidators(null);
    a11.setValidators(null);
    a12.setValidators(null);
    a13.setValidators(null);
    a14.setValidators(null);
    a15.setValidators(null);
    a16.setValidators(null);


    a1.updateValueAndValidity();
    a2.updateValueAndValidity();
    a9.updateValueAndValidity();
    a10.updateValueAndValidity();
    a11.updateValueAndValidity();
    a12.updateValueAndValidity();
    a13.updateValueAndValidity();
    a14.updateValueAndValidity();
    a15.updateValueAndValidity();


  }

  



  }

  changeTypeDocument(event) {
    this.documentTypeSelected = event.value;

    if (this.documentTypeSelected === 'dni') {
      this.maxlengthNumDoc = 8;
      this.placeHolder = 'Ingrese número de DNI';
    } else {
      this.maxlengthNumDoc = 12;

      this.placeHolder = 'Ingrese número de CE';
    }
  }
  changeCodePhone(event){
    this.codePhoneSelected = event.value;
  }
  validar_campo(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }


  cancelar() {
    this.route.navigateByUrl("main/list-boxes");
  }


  validarCelular(event : any): boolean{
    const charCode = (event.which) ? event.which : event.keyCode;
    const numCelular = this.Formulario.get('fm_telefono')?.value;
    var primerDigito = event.target.selectionStart;
    var primerdato = numCelular[0];
    if(primerDigito == 0   ){
      if(charCode == 57 ){
        return true;
      }else{
        return false;
      }
    }else{
      if( charCode > 31 && (charCode < 48 || charCode > 57)){
        return false;
      }else {
        return true;
      }
    }
  }
  validarCelularRep(event : any): boolean{
    const charCode = (event.which) ? event.which : event.keyCode;
    const numCelular = this.Formulario.get('fm_celular_rep')?.value;
    var primerDigito = event.target.selectionStart;
    var primerdato = numCelular[0];
    if(primerDigito == 0   ){
      if(charCode == 57 ){
        return true;
      }else{
        return false;
      }
    }else{
      if( charCode > 31 && (charCode < 48 || charCode > 57)){
        return false;
      }else {
        return true;
      }
    }
  }

  validardomicilio(e : any, idInput: string){
    var value = this.Formulario.get('fm_direccion')?.value;

    let inicio = this.renderer.selectRootElement(`#${idInput}`).selectionStart;
    let fin = this.renderer.selectRootElement(`#${idInput}`).selectionEnd;
    if (e.metaKey || e.ctrlKey) {
      return true;
    }
    if(inicio == 0 && e.key === ' ') return false;
    
    this.Formulario.get('fm_direccion')?.setValue(value.replace(/ {2,}/g, ' '));
    this.renderer.selectRootElement(`#${idInput}`).setSelectionRange(inicio, fin, 'none');


return true;
   }


   download = async (path: string, name: string) => {
    try {
      this.load = true;
      const file = await this.usuarioService.download(path).toPromise();
      this.funcionesService.downloadFile(file, name);
      this.load = false;
    } catch (error) {
      this.load = false;

    }
   
  }


}
