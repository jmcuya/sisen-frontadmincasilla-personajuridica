import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators  } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { FileUploadValidators } from '@iplab/ngx-file-upload';
import codePhone from 'src/app/constants/codePhone';
import { TypeDocument } from 'src/app/models/notifications/notification';
import { Departamento, Distrito, Provincia } from 'src/app/models/ubigeo';
import { UserDetail } from 'src/app/models/users/user';
import { SeguridadService } from 'src/app/services/seguridad.service';
import { UbigeoService } from 'src/app/services/ubigeo.service';
import { UserService } from 'src/app/services/user.service';
import { LBL_ADD_FILES, LBL_ERROR_MAX_FILES, LBL_ERROR_MAX_LENGTH_NAME, LBL_ERROR_MAX_SIZE_FILE, LBL_ERROR_ONLY_FILE, LBL_FEATURES_FILE, MAXFILES, MAX_LENGTH_NAME_FILES, MAX_TAM_FILES_10, MIN_TAM_FILES } from 'src/app/shared/constantes';
import { Profile } from 'src/app/transversal/enums/global.enum';
import { FuncionesService } from 'src/app/utils/funciones.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-add-representative',
  templateUrl: './add-representative.component.html',
  styleUrls: ['./add-representative.component.scss'],
})
export class AddRepresentativeComponent implements OnInit {
  departamentoList: Array<Departamento> = []
  provinciaList: Array<Provincia> = []
  distritoList: Array<Distrito> = []

  maxlengthNumDoc: number;
  minlengthNumDoc: number;
  maxlengthNumDocRep: number;
  minlengthNumDocRep: number;
  
  inputDisabled = false;
  documentTypeSelected: string = '';
  placeHolder = 'Ingrese número de documento';
  user : any ;
  esInterno: boolean = false;
  id;
  public data: any
  codePhone = codePhone;
  Formulario: FormGroup = this.fb.group({
    fm_optiontipo: this.fb.control({
      value: '',// this.data ? this.data.doc_type : '',
      disabled: true,
    }),
    fm_numerodoc: this.fb.control(
      {
        value: '',//this.data ? this.data.doc : '',
        disabled: true,
      },
      [Validators.pattern('^[0-9]+$')]
    ),
    fm_phone_code : this.fb.control({value:''}),
    fm_phone: this.fb.control({
      value:''
    }),
    fm_paginaweb: this.fb.control({
      value:'',
    }),
    // fm_especifique: this.fb.control({
    //   value:'',
    // }),
    fm_nombres: this.fb.control({
      value:'',// this.data ? this.data.name : '',
      disabled: this.inputDisabled,
    }),
    fm_organization_name: this.fb.control({
      value:'',
      disabled: true
    }),
    // fm_apellidos: this.fb.control({
    //   value: this.data ? this.data.lastname : '',
    //   disabled: this.inputDisabled,
    // }),
    fm_apellidoPaterno: this.fb.control({
      value:'',// this.data ? this.data.lastname : '',
      disabled: this.inputDisabled,
    }),
    fm_apellidoMaterno: this.fb.control({
      value:'',// this.data ? this.data.second_lastname : '',
      disabled: this.inputDisabled,
    }),
    fm_correo: this.fb.control(
      {
        value:'',// this.data ? this.data.email : '',
        disabled: this.inputDisabled,
      },
      [
        Validators.required,
        Validators.pattern(
          '[a-zA-Z0-9.+-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}'
        ),
      ]
    ),
    fm_telefono: this.fb.control(
      {
        value: '',//this.data ? this.data.profile : '',
        disabled: this.inputDisabled,
      },
      [Validators.required]
    ),
    fm_direccion: this.fb.control(
      {
        value: '',//this.data ? this.data.profile : '',
        disabled: this.inputDisabled,
      },
      [Validators.required]
    ),
    fm_departamento: this.fb.control(
      {
        value: '',//this.data ? this.data.profile : '',
        disabled: false,
      },
      [Validators.required]
    ),
    fm_provincia: this.fb.control(
      {
        value: '',//this.data ? this.data.profile : '',
        disabled: false,
      },
      [Validators.required]
    ),
    fm_distrito: this.fb.control(
      {
        value: '',//this.data ? this.data.profile : '',
        disabled: false,
      },
      [Validators.required]
    ),
  });
  type:string;

  tipoDocumentoAdjunto  = [
    {id: '1', value: 'Documento que acredita su representación en la Organización Política'},
    {id: '2', value: 'Documento que acredita su representación en la Institución Pública'},
    {id: '3', value: 'Certificado de vigencia de poder en la Empresa privada'},
    {id: '4', value : 'Otros'}
  ]
  /*OLD tipoDocumentoAdjunto  = [
    {id: 'r', value: 'Resolución de designación'},
    {id: 'h', value: 'Historial de afiliación - ROP'},
    {id: 'o' , value : 'Otros'}
  ]*/
  uploadedFiles: Array<File> = [];
  uploadedFilesRep: Array<File> = [];
  errmaxLengthName: boolean = false;
  errduplicate: boolean = false;
  errorOnlyFile: boolean = false;
  errminSizeFile: boolean = false;
  errmaxSizeFile: boolean = false;
  errmaxFiles: boolean = false;
  lblAddFiles: string = LBL_ADD_FILES;
  lblErrorMaxLengthName : string = LBL_ERROR_MAX_LENGTH_NAME;
  lblErrorOnlyFile: string = LBL_ERROR_ONLY_FILE;
  lblErrorMaxSizeFile: string = LBL_ERROR_MAX_SIZE_FILE;
  lblErrorMaxFiles: string = LBL_ERROR_MAX_FILES;
  maxFiles: number = MAXFILES;
  maxSizeFile: number = MAX_TAM_FILES_10;
  minSizeFile: number = MIN_TAM_FILES;
  maxLengthName: number = MAX_LENGTH_NAME_FILES;
  lblFeaturesFile: string = LBL_FEATURES_FILE;
  txtfechapres : string = '';
  dateMax ="";
  nombres: FormControl = new FormControl({ value: '', disabled: this.inputDisabled });
  apPaterno: FormControl = new FormControl({ value: '', disabled: this.inputDisabled });
  apMaterno: FormControl = new FormControl({ value: '', disabled: this.inputDisabled });
  fm_correo: FormControl = new FormControl({ value: '', disabled: this.inputDisabled }, [Validators.required, Validators.pattern('[a-zA-Z0-9.+-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')]);
  fm_correo_pj: FormControl = new FormControl({ value: '', disabled: this.inputDisabled }, [Validators.required, Validators.pattern('[a-zA-Z0-9.+-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')]);
  fm_direccion: FormControl = new FormControl({ value: '', disabled: this.inputDisabled },[Validators.required, Validators.minLength(5)]);
  fm_especifique : FormControl = new FormControl({ value: '', disabled: true });
  fm_direccion_PJ: FormControl = new FormControl({ value: '', disabled: this.inputDisabled },[Validators.required, Validators.minLength(5)]);
  existData = true;
  documentTypeSelectedRep: string = '';
  isCE: boolean = false;
  isCERep: boolean = false;
  lblNombre: string = 'Nombres';
  lblApPat: string = 'Apellido paterno';
  lblApMat: string = 'Apellido materno';
  load: boolean = false;
  toDay = new Date(); 

  filesControlRep = new FormControl(null, [
    Validators.required,
    FileUploadValidators.accept(['.pdf']),
    FileUploadValidators.filesLimit(1),
    FileUploadValidators.sizeRange({minSize: this.minSizeFile, maxSize: this.maxSizeFile}),
    this.noWhitespaceValidator,
  ]);

  cargos = [
    {id: '1', value: 'Representante Legal'},
    {id: '2', value: 'Personero Legal Titular'},
  ]
  
  /*OLD cargos = [
    {id: '1', value: 'Tesorero'},
    {id: '2', value: 'Presidente'},
    {id: '3', value: 'Presidente de la OEC'},
    {id: '4', value: 'Fundador'},
    {id: '5', value: 'Representante Legal'},
    {id: '6', value: 'Secretario'},
    {id: '7', value: 'Presidente del Comité Electoral'},
    {id: '8', value: 'Otros'},
  ]*/

  FormRepresentative: FormGroup = this.fb.group({
    //fm_tipoDocumentoAdjunto: this.fb.control({value:''}),
    fm_tipoDocumentoAdjunto: this.fb.control({value:'',
        disabled: this.inputDisabled,
      },[Validators.required]),
    filesRep: this.filesControlRep,
    fm_optiontipo_rep: this.fb.control(
      {
        value: null,
        disabled: this.inputDisabled,
      },
      [Validators.required]
    ),
    fm_numerodoc_rep: this.fb.control('', [Validators.required]),
    nombres: this.nombres,
    apPaterno: this.apPaterno,
    apMaterno: this.apMaterno,
    fm_correo: this.fm_correo,
    fm_phone_code: this.fb.control({ value: ''}),
    fm_phone: this.fb.control({ value: '', disabled: this.inputDisabled }, [
      Validators.minLength(6),
      this.validatorRepeatFijo,
    ]),
    fm_celular: this.fb.control({ value: '', disabled: this.inputDisabled }, [
      Validators.required,
      Validators.minLength(9),
      this.validatorRepeatMovil,
    ]),
    fm_departamentoList: this.fb.control(
      {
        value: '',
        disabled: this.inputDisabled,
      },
      [Validators.required]
    ),
    fm_provinciaList: this.fb.control(
      {
        value: '',
        disabled: this.inputDisabled,
      },
      [Validators.required]
    ),
    fm_distritoList: this.fb.control(
      {
        value: '',
        disabled: this.inputDisabled,
      },
      [Validators.required]
    ),
    fm_direccion: this.fm_direccion,
    fm_especifique : this.fm_especifique,
    fm_cargo: this.fb.control(
      {
        value: '',
        disabled: this.inputDisabled,
      },
      [Validators.required]
    ),
  });
  constructor(
    //@Inject(MAT_DIALOG_DATA) public data: any,
    //private dialogRef: MatDialogRef<EditUser1Component>,
    private route: Router,
    private fb: FormBuilder,
    private userService: UserService,
    private funcionesService: FuncionesService,
    private ubigeoService: UbigeoService,
    private renderer: Renderer2,
    private seguridadService: SeguridadService,
    private router: ActivatedRoute,
    private readonly location: Location
  ) {
    this.id = this.router.snapshot.paramMap.get('id');
    this.type =  this.router.snapshot.paramMap.get('type');
  }

  ngOnInit(): void {
   // this.getInfo();
    this.initForm();
    if(this.type === 'pj'){
      this.getInfoPj();
    }else{
      this.getInfoPn();
    }
    this.listarDepartamento()
    // if(this.data.estate_inbox == 'Registro interno') this.esInterno = true;
  }

  typeDocument: TypeDocument[] = [
    { id: 'dni', value: 'DNI' },
    { id: 'ruc', value: 'RUC'},
    { id: 'ce', value: 'Carnet de Extranjería' },
  ];
  typeDocument2: TypeDocument[] = [
    { id: 'dni', value: 'DNI' },
    { id: 'ce', value: 'Carnet de Extranjería' },
  ];
  profiles: TypeDocument[] = [
    { id: Profile.RegistryOperator, value: 'Registrador' },
    { id: Profile.Notifier, value: 'Notificador' },
    { id: Profile.Administrador, value: 'Administrador' },
  ];

  formInvalid(control: string) {
    return (
      this.FormRepresentative.get(control).invalid &&
      (this.FormRepresentative.get(control).dirty ||
        this.FormRepresentative.get(control).touched)
    );
  }
  eShowError = (input, error = null) => {
    if (error.required != undefined) {
      return 'Campo Requerido';
    } else if (error.pattern != undefined) {
      return 'Formato no válido';
    } else if (error.fileSize != undefined) {
      return 'Archivo(s) con peso excedido';
    } else if (error.minlength != undefined) {
      return 'Se requiere '+error.minlength.requiredLength+ ' caracteres como mínimo' ;
    } else {
      return 'Campo inválido';
    }
  };  

  private noWhitespaceValidator(control: FormControl) {
    if (control.value == null) return null;
    if (control.value.length == 0) return null;

    for (let index = 0; index < control.value.length; index++) {
      const str = control.value[index].name;
      var frags = str.split('.');
      var name = frags.splice(0, frags.length - 1).join('.');
      if (name.length > 100) {
        return { whitespace: true };
      }
    }
    return null;
  }
  private validatorRepeatFijo(control: FormControl) {
    if (control.value) {
      var re = new RegExp(/^(\d)\1{9}$/);
      var matches = re.test(control.value);
      return !matches ? null : { invalidName: true };
    } else {
      return null;
    }
  }
  private validatorRepeatMovil(control: FormControl) {
    if (control.value) {
      var re = new RegExp(/^(\d)\1{8,}$/);
      var matches = re.test(control.value);
      return !matches ? null : { invalidName: true };
    } else {
      return null;
    }
  }

  getInfoPj(){
    //const  id = this.data.inbox_id;
    //const  id2 = this.data.id;

    this.userService.getUserDetailPj(this.id, false).subscribe((resp)=>{

      if(!resp.success){
        return;
      }
      this.user = resp.user;
      const phoneSplit = this.user.phone.split('-');
      this.Formulario.controls["fm_optiontipo"].setValue( this.user.doc_type);
      this.Formulario.controls["fm_numerodoc"].setValue( this.user.doc);
      this.Formulario.controls["fm_nombres"].setValue( this.user.name);
      this.Formulario.controls["fm_apellidoPaterno"].setValue( this.user.lastname);
      this.Formulario.controls["fm_apellidoMaterno"].setValue( this.user.second_lastname);
      this.Formulario.controls["fm_correo"].setValue( this.user.email);
      this.Formulario.controls["fm_telefono"].setValue( this.user.cellphone);
      this.Formulario.controls["fm_direccion"].setValue( this.user.address);
      this.Formulario.controls['fm_organization_name'].setValue( this.user.organization_name )
      this.Formulario.controls['fm_phone_code'].setValue(phoneSplit[0])
      this.Formulario.controls['fm_phone'].setValue(phoneSplit[1])
      this.Formulario.controls['fm_paginaweb'].setValue(this.user.paginaweb)

      if(this.user.ubigeo != undefined){
        var cadenaUbigeo = this.user.ubigeo.split("/");
        var dep = cadenaUbigeo[0].trim();
        var prov = cadenaUbigeo[1].trim();
        var dis = cadenaUbigeo[2].trim();
        console.log("dep- prov- dis", dep + " - "+ prov + " - " + dis)  

      
        var foundProv = this.departamentoList.find( departamento => departamento.nodep == dep)
        this.Formulario.controls["fm_departamento"].setValue(foundProv);
      }
 
     }, (error)=>{
       console.error(error)
     })   
  }
  getInfoPn(){
    //const  id = this.data.inbox_id;
    //const  id2 = this.data.id;

    this.userService.getUserDetailPn(this.id, false).subscribe((resp)=>{

      if(!resp.success){
        return;
      }
      this.user = resp.user;
      this.Formulario.controls["fm_optiontipo"].setValue( this.user.doc_type);
      this.Formulario.controls["fm_numerodoc"].setValue( this.user.doc);
      this.Formulario.controls["fm_nombres"].setValue( this.user.name);
      this.Formulario.controls["fm_apellidoPaterno"].setValue( this.user.lastname);
      this.Formulario.controls["fm_apellidoMaterno"].setValue( this.user.second_lastname);
      this.Formulario.controls["fm_correo"].setValue( this.user.email);
      this.Formulario.controls["fm_telefono"].setValue( this.user.cellphone);
      this.Formulario.controls["fm_direccion"].setValue( this.user.address);
      this.Formulario.controls['fm_organization_name'].setValue( this.user.organization_name )

      if(this.user.ubigeo != undefined){
        var cadenaUbigeo = this.user.ubigeo.split("/");
        var dep = cadenaUbigeo[0].trim();
        var prov = cadenaUbigeo[1].trim();
        var dis = cadenaUbigeo[2].trim();
        console.log("dep- prov- dis", dep + " - "+ prov + " - " + dis)  

      
        var foundProv = this.departamentoList.find( departamento => departamento.nodep == dep)
        this.Formulario.controls["fm_departamento"].setValue(foundProv);

      }
 
     }, (error)=>{
       console.error(error)
     })   
  }

  getDepartamento(){
    this.ubigeoService.getDepartamentoList().subscribe((resp)=>{
      this.departamentoList = resp
    })
  }

  newRepresentative(){
    this.route.navigateByUrl(`/main/user/edit/pj/${this.user.id}/representante`,{ state: this.user});
  }

  listarDepartamento(){    
    this.seguridadService.getDepartamentoList().subscribe(resp=>{
      this.departamentoList=resp;
    })
  }

  async cambiarProvincia() {
    this.FormRepresentative.get("fm_provinciaList")?.reset("");
    this.FormRepresentative.get("fm_distritoList")?.reset("");
    var value  = this.FormRepresentative.get('fm_departamentoList')?.value.ubdep;    
    this.seguridadService.getProvinciaList(value).subscribe(resp=>{
      this.provinciaList=resp;
    })
    this.distritoList = []    
  }

  async cambiarDistrito() {
    this.FormRepresentative.get("fm_distritoList")?.reset("");
    var valueprovincia = this.FormRepresentative.get('fm_provinciaList')?.value.ubprv
    var valuedepar = this.FormRepresentative.get('fm_departamentoList')?.value.ubdep
    this.seguridadService.getDistritoList(valuedepar, valueprovincia).subscribe(resp=>{
      this.distritoList=resp;
    })
  }
  saveEdit(){

    //console.log(this.FormRepresentative.value);

    if (!this.FormRepresentative.valid) return;
    const fd = new FormData();
    const ubigeo = this.FormRepresentative.controls['fm_departamentoList'].value.nodep + " / "+ this.FormRepresentative.controls['fm_provinciaList'].value.noprv + " / " +this.FormRepresentative.controls['fm_distritoList'].value.nodis;

    var files = this.FormRepresentative.controls['filesRep'].value;

    for (let index = 0; index < files.length; index++) {
      var str1 = files[index].name.replace(/.([^.]*)$/, '.pdf');
      const tempFile = new File(
        [files[index]],
        str1, //str1.replace(/[^a-zA-Z0-9\\.\\-]/g, '-'),
        {
          type: files[index].type.toLowerCase(),
        }
      );
      fd.append('file' + (index + 1), tempFile); //comentado: No se envía al endpoint comentado
      //fd.append('filePhoto', tempFile); 
    }
    if( this.apPaterno.value=="" && this.apMaterno.value=="" ) {
      this.funcionesService.mensajeError("Ingrese por lo menos un apellido");
    } else {

    fd.append('docType', this.FormRepresentative.controls['fm_optiontipo_rep'].value);
    fd.append('doc', this.FormRepresentative.controls['fm_numerodoc_rep'].value);
    fd.append('names', this.nombres.value );
    fd.append('lastname', this.apPaterno.value );
    fd.append('second_lastname', this.apMaterno.value );    
    fd.append('email', this.fm_correo.value);
    fd.append('cellphone', this.FormRepresentative.controls['fm_celular'].value );
    //fd.append('phone', this.FormRepresentative.controls['fm_phone_code'].value!=null?this.FormRepresentative.controls['fm_phone_code'].value:""+"-"+this.FormRepresentative.controls['fm_phone'].value!=null?this.FormRepresentative.controls['fm_phone'].value:"" );
    fd.append('phone', this.FormRepresentative.controls['fm_phone'].value );
    fd.append('ubigeo', ubigeo);
    fd.append('address', this.FormRepresentative.controls['fm_direccion'].value);
    fd.append('position', this.FormRepresentative.controls['fm_cargo'].value.id);
    fd.append('positionName', this.FormRepresentative.controls['fm_cargo'].value.value);
    fd.append('documentTypeAttachment', this.FormRepresentative.controls['fm_tipoDocumentoAdjunto'].value.id);
    fd.append('documentNameAttachment', this.FormRepresentative.controls['fm_tipoDocumentoAdjunto'].value.id == '4' ?  this.FormRepresentative.controls['fm_especifique'].value :  this.FormRepresentative.controls['fm_tipoDocumentoAdjunto'].value.value);
    //fd.append('file1',this.FormRepresentative.controls['filesRep'].value);
    fd.append('userId', this.id)

    new Response(fd).text().then(console.log)

    this.load = true;
    this.userService.SaveRepresentative(fd).subscribe(
      (res) =>{
        this.load = false;
        console.log(res)
        if (res.success) {
          this.funcionesService.mensajeOk(
            'Se creó un representante con éxito'
          );
          this.route.navigateByUrl(`/main/list-boxes`);
        } else {
          this.funcionesService.mensajeError(res.error.message);
        }
      },
      (err) => {
        this.funcionesService.mensajeError(err);
      },
      () => {
        this.load = false;
      }
    )

    }
  }

  initForm() {
    // this.FormRepresentative = this.fb.group({
    //   fm_optiontipo: this.fb.control({
    //     value: '',// this.data ? this.data.doc_type : '',
    //     disabled: true,
    //   }),
    //   fm_numerodoc: this.fb.control(
    //     {
    //       value: '',//this.data ? this.data.doc : '',
    //       disabled: true,
    //     },
    //     [Validators.pattern('^[0-9]+$')]
    //   ),
    //   fm_nombres: this.fb.control({
    //     value:'',// this.data ? this.data.name : '',
    //     disabled: this.inputDisabled,
    //   }),
    //   fm_organization_name: this.fb.control({
    //     value:'',
    //     disabled: true
    //   }),
    //   // fm_apellidos: this.fb.control({
    //   //   value: this.data ? this.data.lastname : '',
    //   //   disabled: this.inputDisabled,
    //   // }),
    //   fm_apellidoPaterno: this.fb.control({
    //     value:'',// this.data ? this.data.lastname : '',
    //     disabled: this.inputDisabled,
    //   }),
    //   fm_apellidoMaterno: this.fb.control({
    //     value:'',// this.data ? this.data.second_lastname : '',
    //     disabled: this.inputDisabled,
    //   }),
    //   fm_correo: this.fb.control(
    //     {
    //       value:'',// this.data ? this.data.email : '',
    //       disabled: this.inputDisabled,
    //     },
    //     [
    //       Validators.required,
    //       Validators.pattern(
    //         '[a-zA-Z0-9.+-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}'
    //       ),
    //     ]
    //   ),
    //   fm_telefono: this.fb.control(
    //     {
    //       value: '',//this.data ? this.data.profile : '',
    //       disabled: this.inputDisabled,
    //     },
    //     [Validators.required]
    //   ),
    //   fm_direccion: this.fb.control(
    //     {
    //       value: '',//this.data ? this.data.profile : '',
    //       disabled: this.inputDisabled,
    //     },
    //     [Validators.required]
    //   ),
    //   fm_departamento: this.fb.control(
    //     {
    //       value: '',//this.data ? this.data.profile : '',
    //       disabled: false,
    //     },
    //     [Validators.required]
    //   ),
    //   fm_provincia: this.fb.control(
    //     {
    //       value: '',//this.data ? this.data.profile : '',
    //       disabled: false,
    //     },
    //     [Validators.required]
    //   ),
    //   fm_distrito: this.fb.control(
    //     {
    //       value: '',//this.data ? this.data.profile : '',
    //       disabled: false,
    //     },
    //     [Validators.required]
    //   ),
    // });
    // if(this.data.estate_inbox === 'Registro interno'){
    //   this.FormRepresentative.get('fm_departamento').clearValidators();
    //   this.FormRepresentative.get('fm_provincia').clearValidators();
    //   this.FormRepresentative.get('fm_distrito').clearValidators();
    //   this.FormRepresentative.get('fm_departamento').updateValueAndValidity();
    //   this.FormRepresentative.get('fm_provincia').updateValueAndValidity();
    //   this.FormRepresentative.get('fm_distrito').updateValueAndValidity();
    // }
    this.getDepartamento();
  }

  private buildError = (message: string) => {
    this.funcionesService.mensajeError(message);
  };
  private buildInfo = (message: string) => {
    this.funcionesService.mensajeInfo(message);
  }
  private validDocument = async (type: string) => {
    var isGeneral = type == 'general';
    var isRepresentante = type == 'representante';
    // if (!this.FormRepresentative.controls['fm_optiontipo'].valid && isGeneral) {
    //   this.buildError('Debe seleccionar un tipo de documento');
    //   return false;
    // }
    // if (!this.FormRepresentative.controls['fm_numerodoc'].valid && isGeneral) {
    //   this.buildError('Debe ingresar un número correcto');
    //   return false;
    // }
    if (
      !this.FormRepresentative.controls['fm_optiontipo_rep'].valid &&
      isRepresentante
    ) {
      this.buildError('Debe seleccionar un tipo de documento Representante');
      return false;
    }
    if (
      !this.FormRepresentative.controls['fm_numerodoc_rep'].valid &&
      isRepresentante
    ) {
      this.buildError('Debe ingresar un número correcto Representante');
      return false;
    }
    return true;
  };
  private consultaSunat = (doc: string) => {
    return new Promise<boolean>((resolve) => {
      this.userService.ConsultaSunat(doc).subscribe(
        (resp) => {
          if (resp) {
            if (resp.list.multiRef.ddp_nombre.$ != undefined) {
              var razon = `${resp.list.multiRef.ddp_nombre.$}`;
              this.FormRepresentative.get('fm_razon_social').setValue(razon);
              resolve(true);
            } else {
              resolve(false);
            }
          } else {
            resolve(false);
          }
        },
        (error) => {
          resolve(false);
        }
      );
    });
  };
  private consultaExtranjeria = (doc: string, type:string) => {
    return new Promise<boolean>((resolve) => {
      this.userService.ConsultaCE(doc, type).subscribe(
        (resp) => {
          if (resp.success) {
            this.nombres.setValue(resp.name);
            this.apPaterno.setValue(resp.lastname != null ? resp.lastname: "");
            this.apMaterno.setValue(resp.second_lastname != null ? resp.second_lastname: "");
            resolve(true);
          } else {
            resolve(false);
          }
        },
        (error) => {
          resolve(false);
        }
      )
    });
  };
  private eSearchDocument = async (type: string) => {
    var tipo = '';
    var doc = '';
    this.load = true;
    if (type == 'general') {
      tipo = this.FormRepresentative.controls['fm_optiontipo'].value;
      doc = this.FormRepresentative.controls['fm_numerodoc'].value;
    }
    if (type == 'representante') {
      tipo = this.FormRepresentative.controls['fm_optiontipo_rep'].value;
      doc = this.FormRepresentative.controls['fm_numerodoc_rep'].value;
    }
    var userExist = await this.consultaCasilla(doc, tipo);

    if(!userExist){
      this.buildError('El documento ingresado ya se encuentra registrado');
      this.load = false;
      return;
    }
    var response = null;
    var message = 'No se encontró los datos del documento.';
    switch (tipo) {
      case 'ruc':
        //this.FormRepresentative.get('fm_razon_social').setValue('PRUEBA S.A.C');
        response = await this.consultaSunat(doc);
        message = 'El RUC ' + doc + ' no ha sido encontrado';
        break;
      case 'ce':
        response = await this.consultaExtranjeria(doc, tipo);
        message =
          'Por favor ingrese los datos del CE ' + doc;
        break;
      case 'dni':
        // this.nombres.setValue('nombres');
        // this.apPaterno.setValue('apellido paterno');
        // this.apMaterno.setValue('apellido materno');
        response = await this.consultaReniec(doc, type);
        message =
          'El DNI ' + doc + ' no ha sido encontrado en el padrón';
        break;
      default:
        break;
    }
    this.load = false;
    if (response) {
      this.existData = true;
    } else {
      if(tipo == 'ce') {
        this.buildInfo(message);
        this.existData = false;
        this.nombres.setValue('');
        this.apPaterno.setValue('');
        this.apMaterno.setValue('');
      }
      else this.buildError(message);
    }
  };
  private consultaReniec = (doc: string, type: string) => {
    return new Promise<boolean>((resolve) => {
      this.userService.ConsultaReniec(doc).subscribe(
        (resp: any) => {
          if (resp) {
            if(resp.nombres == null && resp.appat == null && resp.apmat == null) resolve(false);
            else {
              this.nombres.setValue(resp.nombres);
              this.apPaterno.setValue(resp.appat != null ? resp.appat: "");
              this.apMaterno.setValue(resp.apmat != null ? resp.apmat: "");
              resolve(true);
            }
          } else {
            resolve(false);
          }
        },
        (error) => {
          resolve(false);
        }
      );
    });
  };
  private consultaCasilla = (doc: string, type:string) => {
    return new Promise<boolean>((resolve) => {
      this.userService.ConsultaCasilla(doc, type).subscribe(
        (resp) => {
          if (resp.success) {
            resolve(true);  
          }else{
            resolve(false);
          }
        },
        (error) => {
          resolve(false);
        }
      );
    });
  };  
  buildHide = (name: string) => {
    const esRuc = true;
    switch (name) {
      case 'fm_razon_social':
        if (esRuc) return true;
        return false;
      case 'fm_optiontipo_rep':
        if (esRuc) return true;
        return false;
      case 'fm_numerodoc_rep':
        if (esRuc) return true;
        return false;
      case 'fm_organizacion':
        if (esRuc) return false;
        return true;
    }
  };
  eSearch = async (type: string) => {
    switch (type) {
      case 'general':
        var res = await this.validDocument(type);
        if (res) this.eSearchDocument(type);
        break;
      case 'representante':
        var res = await this.validDocument(type);
        if (res) this.eSearchDocument(type);
        break;
      default:
        break;
    }
  };
  eChangeDocumentoRep(event) {
    this.eResetForm(6);
    this.existData = true;
    this.documentTypeSelectedRep = event.value;
    console.log("change docu: ",event);
    this.isCERep = this.documentTypeSelectedRep === 'ce';
    if (this.documentTypeSelectedRep === 'dni') {
      this.minlengthNumDocRep = 8;
      this.maxlengthNumDocRep = 8;
      this.changeLabelRequired(false);
      this.eChangeType(false);
      //this.eChangeRequired(false);
    } else if (this.documentTypeSelectedRep === 'ce') {
      this.minlengthNumDocRep = 9;
      this.maxlengthNumDocRep = 9;
      this.changeLabelRequired(true);
      this.eChangeType(true);
      //this.eChangeRequired(false);
    }
  }
  private eResetForm = (level: number) => {
    this.nombres.setValue('');
    this.apPaterno.setValue('');
    this.apMaterno.setValue('');
    this.FormRepresentative.get('fm_numerodoc_rep').setValue('');
    if (level == 6) return;
    this.FormRepresentative.get('fm_razon_social').setValue('');
    this.FormRepresentative.get('fm_optiontipo_rep').setValue(null);
    this.FormRepresentative.get('fm_numerodoc').setValue('');
    if (level == 5) return;
  };
  changeLabelRequired(required: boolean) {
    if(required) {
      this.lblNombre = "Nombres*";
    } else {
      this.lblNombre = "Nombres";
    }
  }
  private eChangeType = (status) => {
    let required = status ? [
          Validators.required,
          Validators.pattern(
            "^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$"
          ),
        ] : null;

    let required2 = status ? [
          Validators.pattern(
            "^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$"
          ),
        ] : null;

    this.nombres.setErrors(null);
    this.apPaterno.setErrors(null);
    this.apMaterno.setErrors(null);

    this.nombres.setValidators(required);
    this.apPaterno.setValidators(required2);
    this.apMaterno.setValidators(required2);

    this.nombres.updateValueAndValidity();
    this.apPaterno.updateValueAndValidity();
    this.apMaterno.updateValueAndValidity();
  }

  getNumeroDocumento() {
    this.FormRepresentative.get('fm_numerodoc').valueChanges.subscribe((documento) => {
      if(this.documentTypeSelected == 'dni') {
        if(documento.length == this.minlengthNumDoc){
          this.eSearch('general');
        }
        else {
          this.nombres.setValue('');
          this.apPaterno.setValue('');
          this.apMaterno.setValue('');
        }
      }
      else if(this.documentTypeSelected == 'ruc') {
        if(documento.length == this.minlengthNumDoc){
          this.eSearch('general');
        }
        else {
          this.FormRepresentative.get('fm_razon_social').setValue('');
        }
      }
      else if(this.documentTypeSelected == 'ce') {
        this.existData = true;
        this.nombres.setValue('');
        this.apPaterno.setValue('');
        this.apMaterno.setValue('');
      }
    });
  }

  eChangeDocumentoAttach(event){
    const value = event.value;
    console.log("valueee =>>>" , value)
    if(value.id === '4'){
      this.FormRepresentative.get('fm_especifique').setValidators([Validators.required]);
      this.FormRepresentative.controls['fm_especifique'].enable();
      this.FormRepresentative.controls['fm_especifique'].markAllAsTouched();
    }else{
      this.FormRepresentative.get('fm_especifique').setValidators(null);
     
      this.FormRepresentative.controls['fm_especifique'].disable();
    }
    this.FormRepresentative.get('fm_especifique').updateValueAndValidity();
   
  }

  changeTypeDocument(event) {
    this.documentTypeSelected = event.value;

    if (this.documentTypeSelected === 'dni') {
      this.maxlengthNumDoc = 8;
      this.placeHolder = 'Ingrese número de DNI';
    } else {
      this.maxlengthNumDoc = 12;

      this.placeHolder = 'Ingrese número de CE';
    }
  }
  changeCodePhone(event){
    
  }
  validar_campo(event, type): boolean {

    const charCode = (event.which) ? event.which : event.keyCode;
    var posicion = event.target.selectionStart;
  
    if(posicion == 0  && type === "fm_celular" ){
      if(charCode == 57 ){
        return true;
      }else{
        return false;
      }
    }else{
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
    }
  }
  buscarCE() {
    /*console.log(this.isCE)
    console.log(this.isCERep)*/
    if(this.isCE) this.eSearch('general');
    if(this.minlengthNumDocRep==this.FormRepresentative.controls['fm_numerodoc_rep'].value.length ) this.eSearch('representante');
  }

  soloExpLetras(idInput: string, inputForm: FormControl, e: any) {
    let inicio = this.renderer.selectRootElement(`#${idInput}`).selectionStart;
    let fin = this.renderer.selectRootElement(`#${idInput}`).selectionEnd;
    let value : string = inputForm.value;
    if (e.metaKey || e.ctrlKey) {
      return true;
    }
    if(inicio == 0 && e.key === ' ') return false;
    inputForm.setValue(value.replace(/ {2,}/g, ' '));
    this.renderer.selectRootElement(`#${idInput}`).setSelectionRange(inicio, fin, 'none');
    return !!/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/.test(e.key);
  }

  quitarDobleEspacio(idInput: string, inputForm: FormControl, e: any) {
    let inicio = this.renderer.selectRootElement(`#${idInput}`).selectionStart;
    let fin = this.renderer.selectRootElement(`#${idInput}`).selectionEnd;
    let value : string = inputForm.value;
    if (e.metaKey || e.ctrlKey) {
      return true;
    }
    if(inicio == 0 && e.key === ' ') return false;
    inputForm.setValue(value.replace(/ {2,}/g, ' '));
    this.renderer.selectRootElement(`#${idInput}`).setSelectionRange(inicio, fin, 'none');
  }
  validar_campo_phone(event, type): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode===45) {return true;}
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  cambiarCargo(){
    console.log(this.FormRepresentative)
  }


  cancelar(): void {
    this.location.back();
  }


  validarCelular(event : any): boolean{
    const charCode = (event.which) ? event.which : event.keyCode;
    const numCelular = this.FormRepresentative.get('fm_telefono')?.value;
    var primerDigito = event.target.selectionStart;
    var primerdato = numCelular[0];
    if(primerDigito == 0   ){
      if(charCode == 57 ){
        return true;
      }else{
        return false;
      }
    }else{
      if( charCode > 31 && (charCode < 48 || charCode > 57)){
        return false;
      }else {
        return true;
      }
    }
  }

  validardomicilio(e : any, idInput: string){
    var value = this.FormRepresentative.get('fm_direccion')?.value;

    let inicio = this.renderer.selectRootElement(`#${idInput}`).selectionStart;
    let fin = this.renderer.selectRootElement(`#${idInput}`).selectionEnd;
    if (e.metaKey || e.ctrlKey) {
      return true;
    }
    if(inicio == 0 && e.key === ' ') return false;
    
    this.FormRepresentative.get('fm_direccion')?.setValue(value.replace(/ {2,}/g, ' '));
    this.renderer.selectRootElement(`#${idInput}`).setSelectionRange(inicio, fin, 'none');


return true;
   }

}
